from django import forms, VERSION
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django_comments.forms import CommentDetailsForm
from mptt_comments.models import MPTTComment
from tinymce.widgets import TinyMCE
from captcha.fields import ReCaptchaField

COMMENT_MAX_LENGTH = getattr(settings, 'COMMENT_MAX_LENGTH', 3000)
COMMENTS_TITLE_MAX_LENGTH = getattr(settings, 'COMMENTS_TITLE_MAX_LENGTH', 255)


class MPTTCommentForm(CommentDetailsForm):
    email = forms.EmailField(label=_("Email address"), required=False)
    honeypot = forms.CharField(required=False,
                               label=_('If you enter anything in this field '
                                       'your comment will be treated as spam'))
    title = forms.CharField(label=_('Title'), required=False, max_length=COMMENTS_TITLE_MAX_LENGTH)
    parent = forms.ModelChoiceField(queryset=MPTTComment.objects.all(), required=False, widget=forms.HiddenInput)
    comment = forms.CharField(label=_('Comment'), widget=TinyMCE(), max_length=COMMENT_MAX_LENGTH)
    captcha = ReCaptchaField(attrs={'theme': 'clean'})

    def __init__(self, target_object, parent=None, data=None, initial=None):
        from collections import OrderedDict
        keys = list(self.base_fields.keys())
        keys.remove('title')
        keys.insert(keys.index('comment'), 'title')
        self.base_fields = OrderedDict((k, self.base_fields[k]) for k in keys)
        self.parent = parent
        if initial is None:
            initial = {}
        initial.update({'parent': self.parent})
        super(MPTTCommentForm, self).__init__(target_object, data=data, initial=initial)

    def clean_honeypot(self):
        """
        Check that nothing's been entered into the honeypot.
        """
        value = self.cleaned_data['honeypot']
        if value:
            raise forms.ValidationError(self.fields['honeypot'].label)
        return value

    def full_clean(self):
        """
        The captcha is for anonymous users only.
        """
        super().full_clean()
        user = self.data.get('user')
        if user:
            del self._errors['captcha']

    def get_comment_model(self):
        return MPTTComment

    def get_comment_create_data(self):
        d = super(MPTTCommentForm, self).get_comment_create_data()
        d['parent'] = self.cleaned_data['parent']
        d['title'] = self.cleaned_data['title']
        return d
