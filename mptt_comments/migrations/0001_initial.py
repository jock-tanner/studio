# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models
from django.conf import settings
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        ('sites', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='MPTTComment',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('object_pk', models.TextField(verbose_name='object ID')),
                ('title', models.TextField(verbose_name='Title', blank=True)),
                ('tree_path', models.CharField(verbose_name='Tree path', db_index=True, max_length=500, editable=False)),
                ('user_name', models.CharField(verbose_name="user's name", max_length=50, blank=True)),
                ('user_email', models.EmailField(verbose_name="user's email address", max_length=254, null=True, blank=True)),
                ('user_url', models.URLField(verbose_name="user's URL", blank=True)),
                ('comment', tinymce.models.HTMLField(verbose_name='comment', max_length=3000)),
                ('submit_date', models.DateTimeField(verbose_name='date/time submitted', default=None)),
                ('ip_address', models.GenericIPAddressField(verbose_name='IP address', unpack_ipv4=True, null=True, blank=True)),
                ('is_public', models.BooleanField(help_text='Uncheck this box to make the comment effectively disappear from the site.', verbose_name='is public', default=True)),
                ('is_removed', models.BooleanField(help_text='Check this box if the comment is inappropriate. A "This comment has been removed" message will be displayed instead.', verbose_name='is removed', default=False)),
                ('content_type', models.ForeignKey(verbose_name='content type', related_name='content_type_set_for_bettercomment', to='contenttypes.ContentType')),
                ('last_child', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, null=True, verbose_name='Last child', to='mptt_comments.MPTTComment')),
                ('parent', models.ForeignKey(blank=True, null=True, related_name='children', verbose_name='Parent', default=None, to='mptt_comments.MPTTComment')),
                ('site', models.ForeignKey(to='sites.Site')),
                ('user', models.ForeignKey(blank=True, null=True, related_name='bettercomment_comments', verbose_name='user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('tree_path',),
                'verbose_name': 'Threaded comment',
                'verbose_name_plural': 'Threaded mptt_comments',
            },
        ),
    ]
