from django.db import models
from django.forms.models import model_to_dict
from django_comments import signals
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from mptt_comments.forms import MPTTCommentForm


@api_view(['POST', ])
def submit(request):
    """
    POST − submit comment
    PUT − edit comment
    """
    if request.method == 'POST':
        post_data = request.data.copy()
        if request.user.is_authenticated():
            post_data['name'] = request.user.get_full_name() or request.user.get_username()
            post_data['user'] = request.user.pk
        else:
            post_data.pop('user', None)
        content_type = post_data.get('content_type')
        object_pk = post_data.get("object_pk")
        try:
            model = models.get_model(*content_type.split(".", 1))
            commented_object = model.objects.get(pk=object_pk)
        except Exception as e:
            return Response(e, status=status.HTTP_400_BAD_REQUEST)
        comment_form = MPTTCommentForm(commented_object, data=post_data)
        if comment_form.security_errors():
            return Response(comment_form.security_errors(), status=status.HTTP_400_BAD_REQUEST)
        if comment_form.errors:
            return Response(comment_form.errors, status=status.HTTP_400_BAD_REQUEST)

        comment = comment_form.get_comment_object()
        comment.ip_address = request.META.get("REMOTE_ADDR", None)
        if request.user.is_authenticated():
            comment.user = request.user
        comment.save()
        # only this signal is supported by now
        signals.comment_was_posted.send(
            sender=comment.__class__,
            comment=comment,
            request=request
        )
        return Response(model_to_dict(comment))
