import os
from datetime import timedelta

from django.utils.translation import ugettext_lazy as _

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

DEBUG = False
THUMBNAIL_DEBUG = False

SECRET_KEY = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

ALLOWED_HOSTS = [
    'www.codetinkers.ru',
    'codetinkers.ru',
    '127.0.0.1',
]

INSTALLED_APPS = [
    'grappelli',
    'modeltranslation',
    'filebrowser',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_comments',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'django.contrib.humanize',
    'django.contrib.sitemaps',
    'rosetta',
    'social.apps.django_app.default',
    'account',
    'avatar',
    'django_bleach',
    'tinymce',
    'sorl.thumbnail',
    'mce_filebrowser',
    'sitetree',
    'bootstrapform',
    'pinax_theme_bootstrap',
    'bootstrap_pagination',
    'mathfilters',
    'haystack',
    'mptt',
    'captcha',
    'rest_framework',
    'text_blocks',
    'navigation',
    'blog',
    'bootstrap_calendar',
    'mptt_comments',
]

COMMENTS_APP = 'mptt_comments'

MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'account.middleware.TimezoneMiddleware',
]

AUTHENTICATION_BACKENDS = [
    'social.backends.google.GoogleOAuth2',
    'social.backends.bitbucket.BitbucketOAuth2',
    'social.backends.github.GithubOAuth2',
    'django.contrib.auth.backends.ModelBackend',
]

LOGIN_URL = '/account/login/'

ACCOUNT_OPEN_SIGNUP = False

SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = [
    'username',
    'first_name',
    'email',
]
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = 'xxxxxxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.apps.googleusercontent.com'
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = 'xxxxxxxxxxxxxxxxxxxxxxxx'
SOCIAL_AUTH_BITBUCKET_OAUTH2_KEY = 'xxxxxxxxxxxxxxxxxx'
SOCIAL_AUTH_BITBUCKET_OAUTH2_SECRET = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
SOCIAL_AUTH_GITHUB_OAUTH2_KEY = 'xxxxxxxxxxxxxxxxxxxx'
SOCIAL_AUTH_GITHUB_OAUTH2_SECRET = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

ROOT_URLCONF = 'studio.urls'

WSGI_APPLICATION = 'wsgi.application'

SITE_ID = 1

USE_TZ = True
TIME_ZONE = 'Asia/Vladivostok'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'studio',
        'USER': 'studio',
        'PASSWORD': 'xxxxxxxxxxxx',
        'HOST': '127.0.0.1',
        'PORT': '',
    },
}

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates/'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'social.apps.django_app.context_processors.backends',
                'social.apps.django_app.context_processors.login_redirect',
                'account.context_processors.account',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'studio.context_processors.site',
            ],
        },
    },
]

SITETREE_MODEL_TREE_ITEM = 'navigation.TranslatableTreeItem'

LANGUAGE_CODE = 'en_us'
USE_I18N = True
USE_L10N = True
LANGUAGES = [
    ('en', _('English'), ),
    ('ru', _('Russian'), ),
]
LOCALE_PATHS =[
    os.path.join(BASE_DIR, 'locales/')
]

STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'site_static/'),
]
STATIC_ROOT = os.path.join(BASE_DIR, '../static/')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, '../media/')
FILE_UPLOAD_DIRECTORY_PERMISSIONS = 0o775
FILE_UPLOAD_PERMISSIONS = 0o664
FILEBROWSER_DEFAULT_PERMISSIONS = 0o664
FILEBROWSER_DIRECTORY = 'uploads/'
FILEBROWSER_VERSIONS_BASEDIR = '_versions/'

LOGIN_REDIRECT_URL = '/account/settings/'

TINYMCE_DEFAULT_CONFIG = {
    'theme': 'advanced',
    'relative_urls': False,
    'plugins': 'autoresize',
    'width': '100%',
    'autoresize_max_height': 360,
    'theme_advanced_buttons1': 'undo,redo,|,bold,italic,underline,strikethrough,sup,sub,fontselect,fontsizeselect,forecolor,backcolor',
    'theme_advanced_buttons2': 'styleselect,link,unlink,image,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,outdent,indent,|,blockquote,|,code',
    'file_browser_callback': 'mce_filebrowser',
}
TINYMCE_JS_ROOT = os.path.join(STATIC_ROOT, 'tinymce/')

THUMBNAIL_ENGINE = 'sorl.thumbnail.engines.pil_engine.Engine'
THUMBNAIL_KVSTORE = 'sorl.thumbnail.kvstores.redis_kvstore.KVStore'
THUMBNAIL_REDIS_HOST = '127.0.0.1'
THUMBNAIL_REDIS_PORT = 6379
THUMBNAIL_REDIS_DB = 3
THUMBNAIL_DBM_MODE = 0o664
THUMBNAIL_QUALITY = 90

BLEACH_ALLOWED_TAGS = ['p', 'b', 'br', 'i', 'strong', 'em', 'u', ]
BLEACH_ALLOWED_ATTRIBUTES = []
BLEACH_ALLOWED_STYLES = []
BLEACH_STRIP_TAGS = True
BLEACH_STRIP_COMMENTS = True

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(BASE_DIR, '../whoosh_index'),
        'EXCLUDED_INDEXES': [
            'blog.search_indexes.PostRuIndex',
            'blog.search_indexes.FlatPageRuIndex',
        ],
    },
    'default_ru': {
        'ENGINE': 'studio.search.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(BASE_DIR, '../whoosh_index_ru'),
        'EXCLUDED_INDEXES': [
            'blog.search_indexes.PostIndex',
            'blog.search_indexes.FlatPageIndex',
        ],
    },
}
HAYSTACK_ROUTERS = [
    'studio.search.search.MultilingualRouter',
]
HAYSTACK_SEARCH_RESULTS_PER_PAGE = 10

RECAPTCHA_PUBLIC_KEY = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
RECAPTCHA_PRIVATE_KEY = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
RECAPTCHA_USE_SSL = False

AVATAR_GRAVATAR_DEFAULT = 'mm'
AVATAR_MAX_AVATARS_PER_USER = 5

BROKER_URL = 'redis://localhost:6379/3'
CELERYBEAT_SCHEDULE = {
    'update-index': {
        'task': 'studio.blog.tasks.do_update_indexes',
        'schedule': timedelta(minutes=15),
    },
}

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_DEFAULT_FROM = 'me@my.host'

# custom settings
BLOG_POSTS_PER_PAGE = 5
BLOG_REPOST_ATTEMPTS_LIMIT = 5
BLOG_REPOST_SIGNATURE = '<p style="font-size:75%">Reposted from <a href="{0}">Code Tinkers Blog</a>.</p>'
BLOG_REPOST_LJ_XMLRPC_URL = r'http://www.livejournal.com/interface/xmlrpc'
BLOG_LJ_USERNAME = 'xxxxxxxxxxxx'
BLOG_LJ_PASSWORD = 'xxxxxxxxxxxx'
