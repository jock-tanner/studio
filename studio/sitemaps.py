from django.core.urlresolvers import reverse
from django.contrib.sitemaps import Sitemap
from django.contrib.flatpages.sitemaps import FlatPageSitemap

from blog.models import Post


class CustomFlatPageSitemap(FlatPageSitemap):
    priority = 0.7


class StaticViewSitemap(Sitemap):
    changefreq = 'monthly'
    priority = 0.5

    @staticmethod
    def items():
        return ['home', ]

    @staticmethod
    def location(obj):
        return reverse(obj)


class BlogSitemap(Sitemap):
    changefreq = 'daily'
    priority = 1.0

    @staticmethod
    def items():
        return Post.objects.filter(is_draft=False)

    @staticmethod
    def lastmod(obj):
        return obj.edited


sitemaps = {
    'StaticViews': StaticViewSitemap,
    'FlatPages': CustomFlatPageSitemap,
    'Blog': BlogSitemap,
}
