from django.contrib.syndication.views import Feed
from django_bleach.utils import get_bleach_default_options
import bleach

from blog.models import Post


class BlogFeed(Feed):
    """
    This feed is language agnostic (yet?).
    """
    title = 'Code Tinkers Blog'
    link = 'https://www.codetinkers.ru/blog/'
    description = 'Latest posts'

    @staticmethod
    def items():
        return Post.objects.filter(is_draft=False)[:5]

    def item_title(self, item):
        return item.title

    def item_link(self, item):
        return item.get_absolute_url()

    def item_description(self, item):
        text = bleach.clean(item.text, **get_bleach_default_options())
        if len(text) > 1024:
            return text[:1023] + '…'
        return text
