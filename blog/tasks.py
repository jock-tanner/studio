from xmlrpc.client import ServerProxy, Error as XmlrpcError
from hashlib import md5

from django.db import transaction
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from haystack.management.commands import update_index
from celery import shared_task

from mptt_comments.models import MPTTComment
from blog.models import Post, Subscription


def text_for_repost(post):
    """
    Deletes line breaks and inserts signature.

    :param post: Post object.
    """
    url = ''.join(['https://', get_current_site(None).domain, post.get_absolute_url()])
    signature = getattr(settings, 'BLOG_REPOST_SIGNATURE', '<a href="{0}">Reposted</a>.')
    text = post.text_ru if post.text_ru else post.text
    text = text.replace('\r', '')
    return text + signature.format(url)


@shared_task
def do_repost_to_lj(post_id):
    """
    Makes a repost to LiveJournal.

    :param post_id: the ID of the post to repost.
    """
    post = Post.objects.get(id=post_id)
    if post.repost_attempts > getattr(settings, 'BLOG_REPOST_ATTEMPTS_LIMIT', 5):
        post.repost_status = 3
        post.save()
        transaction.commit()
        return 'Failed to repost'
    try:
        lj_server = ServerProxy(settings.BLOG_REPOST_LJ_XMLRPC_URL)
        challenge_result = lj_server.LJ.XMLRPC.getchallenge()
        data = {
            'username': settings.BLOG_LJ_USERNAME,
            'auth_method': 'challenge',
            'ver': '1',
            'auth_challenge': challenge_result['challenge'],
            'auth_response': md5(challenge_result['challenge'].encode('utf-8') +
                                 md5(settings.BLOG_LJ_PASSWORD.encode('utf-8')).hexdigest().encode('utf-8')).hexdigest(),
            'event': text_for_repost(post),
            'security': 'private' if post.is_draft else 'public',
            'subject': post.title_ru if post.title_ru else post.title,
            'year': post.created.year,
            'mon': post.created.month,
            'day': post.created.day,
            'hour': post.created.hour,
            'min': post.created.minute,
        }
        if post.repost_itemid:
            data.update({
                'itemid': post.repost_itemid,
            })
            post_result = lj_server.LJ.XMLRPC.editevent(data)
        else:
            post_result = lj_server.LJ.XMLRPC.postevent(data)
        post.repost_itemid = post_result['itemid']
        post.repost_url = post_result['url']
        post.repost_status = 2
    except XmlrpcError:
        post.repost_attempts += 1
        post.repost_status = 1
        post.repost_url = None
        post.repost_itemid = None
        raise
    finally:
        post.save()
        transaction.commit()
    return post_result


@shared_task
def do_notify_subscribers(subscriber_id, content_object_id):
    """
    Notifies the subscribers about the event.

    :param subscriber: the ID of the user (AUTH_USER_MODEL item) that subscribes to the event,
    :param content_object: the ID of the underlying object (only MPTTComment item by now).
    """
    subscriber = get_user_model().objects.get(id=subscriber_id)
    comment = MPTTComment.objects.get(id=content_object_id)
    ctx = Context({
        'subscriber': subscriber,
        'comment': comment,
        'site': get_current_site(None),
    })
    text_template = get_template('email/post_notify.txt')
    html_template = get_template('email/post_notify.html')
    msg = EmailMultiAlternatives(_('Post “{0}” was commented').format(comment.content_object.title),
                                 text_template.render(ctx),
                                 settings.EMAIL_DEFAULT_FROM,
                                 [
                                     subscriber.email,
                                 ])
    msg.attach_alternative(html_template.render(ctx), 'text/html')
    msg.send()


@shared_task
def do_update_indexes():
    """
    Updates search indices in background. (Launched by Celery beat.)
    """
    update_index.Command().handle(remove=True)
