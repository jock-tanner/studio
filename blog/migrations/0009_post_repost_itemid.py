# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_post_repost_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='repost_itemid',
            field=models.PositiveIntegerField(verbose_name='Repost ID', blank=True, null=True),
        ),
    ]
