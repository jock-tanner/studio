# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_post_is_draft'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='post',
            options={'verbose_name': 'Post', 'ordering': ['-published', '-created'], 'verbose_name_plural': 'Posts'},
        ),
        migrations.AddField(
            model_name='post',
            name='published',
            field=models.DateTimeField(verbose_name='Published', blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='text',
            field=tinymce.models.HTMLField(verbose_name='Text'),
        ),
        migrations.AlterField(
            model_name='post',
            name='text_en',
            field=tinymce.models.HTMLField(verbose_name='Text', null=True),
        ),
        migrations.AlterField(
            model_name='post',
            name='text_ru',
            field=tinymce.models.HTMLField(verbose_name='Text', null=True),
        ),
    ]
