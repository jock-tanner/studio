# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0005_auto_20150829_2322'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostRu',
            fields=[
            ],
            options={
                'ordering': ['-published', '-created'],
                'verbose_name': 'Post',
                'verbose_name_plural': 'Posts',
                'proxy': True,
            },
            bases=('blog.post',),
        ),
    ]
