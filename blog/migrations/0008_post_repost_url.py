# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0007_auto_20151108_0223'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='repost_url',
            field=models.URLField(blank=True, verbose_name='Reposted at', null=True),
        ),
    ]
