# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0009_post_repost_itemid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='repost_url',
            field=models.URLField(null=True, blank=True, verbose_name='Reposted to'),
        ),
    ]
