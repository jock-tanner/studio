# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0012_auto_20151110_0745'),
    ]

    operations = [
        migrations.RenameField(
            model_name='subscription',
            old_name='user',
            new_name='subscriber',
        ),
        migrations.AlterUniqueTogether(
            name='subscription',
            unique_together=set([('subscriber', 'post')]),
        ),
    ]
