# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('flatpages', '0002_auto_20151012_0622'),
        ('blog', '0006_postru'),
    ]

    operations = [
        migrations.CreateModel(
            name='FlatPageRu',
            fields=[
            ],
            options={
                'verbose_name': 'flat page',
                'verbose_name_plural': 'flat pages',
                'proxy': True,
                'db_table': 'django_flatpage',
                'ordering': ('url',),
            },
            bases=('flatpages.flatpage',),
        ),
        migrations.AddField(
            model_name='post',
            name='repost_attempts',
            field=models.SmallIntegerField(verbose_name='Failed repost attempts', default=0),
        ),
        migrations.AddField(
            model_name='post',
            name='repost_status',
            field=models.SmallIntegerField(verbose_name='Repost status', default=1, choices=[(0, 'Do not repost'), (1, 'Scheduled for repost'), (2, 'Reposted'), (3, 'Failed to repost')]),
        ),
    ]
