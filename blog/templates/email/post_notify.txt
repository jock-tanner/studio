{% load i18n %}Hello, {{ subscriber.get_username }}!

We’d like to bring to your attention that the post titled
“{{ comment.content_object.title }}” was commented recently by {% if comment.user %}user {{ comment.user.get_username }}{% else %}anonymous user{% endif %}.

The comment was:
“
{{ comment.comment|striptags }}
”

You can find the post following this link:
    https://{{ site.domain }}{{ comment.content_object.get_absolute_url }}

Sincerely,
{{ site.name }}.