from django.contrib.flatpages.models import FlatPage
from haystack import indexes

from blog.models import Post, PostRu, FlatPageRu


class PostIndex(indexes.SearchIndex, indexes.Indexable):
    """
    Default (‘en’) search index.
    """
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return Post

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(is_draft=False)


class PostRuIndex(indexes.SearchIndex, indexes.Indexable):
    """
    Russian (‘ru’) search index.
    """
    text = indexes.CharField(document=True, use_template=True, template_name='search/indexes/blog/post_text_ru.txt')

    def get_model(self):
        return PostRu

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(is_draft=False)


class FlatPageIndex(indexes.SearchIndex, indexes.Indexable):
    """
    Default (‘en’) search index for FlatPages.
    """
    text = indexes.CharField(document=True, use_template=True,
                             template_name='search/indexes/flatpages/flatpage_text.txt')

    def get_model(self):
        return FlatPage

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(registration_required=False)


class FlatPageRuIndex(indexes.SearchIndex, indexes.Indexable):
    """
    Russian (‘ru’) search index for FlatPages.
    """
    text = indexes.CharField(document=True, use_template=True,
                             template_name='search/indexes/flatpages/flatpage_text_ru.txt')

    def get_model(self):
        return FlatPageRu

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(registration_required=False)
