from django.conf.urls import url

from bootstrap_calendar.views import show_posts

urlpatterns = [
    url(r'^posts/', show_posts, name='posts'),
]
