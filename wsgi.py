import os
import site

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
site.addsitedir(BASE_DIR)
site.addsitedir(os.path.join(BASE_DIR, '../env/lib/python3.4/site-packages'))

ACTIVATE_SCRIPT = os.path.join(BASE_DIR, '../env/bin/activate_this.py')
with open(ACTIVATE_SCRIPT) as exec_file:
    code = compile(exec_file.read(), ACTIVATE_SCRIPT, 'exec')
    exec(code, dict(__file__=ACTIVATE_SCRIPT))

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'studio.settings')

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
