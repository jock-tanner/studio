from modeltranslation.translator import translator, TranslationOptions

from text_blocks.models import TextBlock


class TextBlockTranslationOptions(TranslationOptions):
    fields = ('header', 'content', )

translator.register(TextBlock, TextBlockTranslationOptions)
