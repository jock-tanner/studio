# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TextBlock',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('slug', models.CharField(max_length=255, unique=True, verbose_name='Slug', help_text='A unique name to identify the block')),
                ('header', models.CharField(verbose_name='Header', null=True, max_length=255, blank=True, help_text='An optional header for this content')),
                ('header_en', models.CharField(verbose_name='Header', null=True, max_length=255, blank=True, help_text='An optional header for this content')),
                ('header_ru', models.CharField(verbose_name='Header', null=True, max_length=255, blank=True, help_text='An optional header for this content')),
                ('content', tinymce.models.HTMLField(verbose_name='Content', null=True, blank=True)),
                ('content_en', tinymce.models.HTMLField(verbose_name='Content', null=True, blank=True)),
                ('content_ru', tinymce.models.HTMLField(verbose_name='Content', null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'Text Blocks',
                'verbose_name': 'Text Block',
            },
        ),
    ]
